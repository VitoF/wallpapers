Images containing "garuda" in their name contain artwork (garuda logo) made and owned by [SGS](https://gitlab.com/SGSm) Copyright (C) 2022

Images containing "arch" in their name contain artwork (archlinux logo) made and owned by Arch Linux. The [Arch Linux trademark policy](https://wiki.archlinux.org/title/DeveloperWiki:TrademarkPolicy) is published under the CC-BY-SA license, and the same license applies to such images.


For further details on Copyright and Licensing, please refer to the LICENSE file in this repository.
